package id.lab.entity;


import java.util.Date;

import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PreUpdate;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

@Entity
@Data
public class Customer {
	
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(length = 48)
    @Id
	private String id;
	
	@Column(length = 15)
	private String code;
	
	@Column(length = 50)
	private String name;
	
	@Column(length = 30)
	private String phone;
	
	@Column(nullable = true, updatable = false)
	private Date created = new Date();
	
	@Column(nullable = true)
	private Date modified = new Date();
	
	@PreUpdate
	void beforeUpdate() {
		this.modified = new Date();
	}

}
