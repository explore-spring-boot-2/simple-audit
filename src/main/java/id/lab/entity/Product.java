package id.lab.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.envers.Audited;

import lombok.Data;

@Entity
@Data
@Audited
public class Product {
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(length = 48)
    @Id
	private String id;
	
	@Column(length = 15)
	private String code;
	
	@Column(length = 50)
	private String name;
	
	@Column(length = 40)
	private String barcode;

}
