package id.lab.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import lombok.Data;

@Entity @Data
@SQLDelete(sql = "UPDATE fixed_asset SET deleted = true WHERE id=?")
@Where(clause = "deleted=false")
public class FixedAsset {
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(length = 48)
    @Id
	private String id;
	
	@Column(length = 15)
	private String code;
	
	@Column(length = 50)
	private String name;

	@Column(length = 30)
	private String tipe; // tanah, bangungan, mesin, perabotan, peralatan, perlengkapan, kendaraan
	
	private boolean deleted = Boolean.FALSE;
	
}
