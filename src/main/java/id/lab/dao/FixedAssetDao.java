package id.lab.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import id.lab.entity.FixedAsset;

public interface FixedAssetDao extends PagingAndSortingRepository<FixedAsset, String>{

}
