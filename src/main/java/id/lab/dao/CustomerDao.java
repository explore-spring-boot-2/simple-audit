package id.lab.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import id.lab.entity.Customer;

public interface CustomerDao extends PagingAndSortingRepository<Customer, String>{

}
