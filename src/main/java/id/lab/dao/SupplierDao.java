package id.lab.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import id.lab.entity.Supplier;

public interface SupplierDao extends PagingAndSortingRepository<Supplier, String>{

}
