package id.lab.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import id.lab.entity.Product;

public interface ProductDao extends PagingAndSortingRepository<Product, String>{

}
