package id.lab.entity;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import id.lab.dao.SupplierDao;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootTest
public class SupplierTests {
	
	@Autowired SupplierDao supplierDao;
	
	@Test
	void generateCreatedModifiedSupplier() {
		Supplier supplier = new Supplier();
		supplier.setCode("BBCA");
		supplier.setName("PT Bank Central Asia");
		supplier.setPhone("1500888");
		Assertions.assertNull(supplier.getCreated());
		supplierDao.save(supplier);
		Assertions.assertNotNull(supplier.getCreated());
		log.info(supplier.getCode() + ": " + supplier.getCreated().toString());
	}

}
