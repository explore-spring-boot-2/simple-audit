package id.lab.entity;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import id.lab.dao.FixedAssetDao;


@SpringBootTest
public class FixedAssetTests {
	
	@Autowired FixedAssetDao fixedAssetDao;
	
	@Test
	void insertAndDeleteFixedAsset() {
		FixedAsset fa = new FixedAsset();
		fa.setCode("CW66HE-05170");
		fa.setName("Nissan CW66HE");
		fa.setTipe("Truck");
		fixedAssetDao.save(fa);
		Assertions.assertEquals(false, fa.isDeleted());
		System.out.println(fa.getCode() + ": " + fa.getId());
		fixedAssetDao.delete(fa);
	}

}
