package id.lab.entity;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import id.lab.dao.CustomerDao;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootTest
public class CustomerTests {
	
	@Autowired CustomerDao customerDao;
	
	@Test
	void generateIdCustomer() {
		Customer customer = new Customer();
		customer.setCode("A00001");
		customer.setName("AGUS");
		customer.setPhone("08123");
		Assertions.assertNull(customer.getId());
		customerDao.save(customer);
		Assertions.assertNotNull(customer.getId());
		log.info(customer.getCode() + ": " + customer.getId());
	}

	@Test
	void generateCreatedModifiedCustomer() {
		Customer customer = new Customer();
		customer.setCode("B00001");
		customer.setName("BRYAN");
		customer.setPhone("08156");
		Assertions.assertNotNull(customer.getCreated());
		customerDao.save(customer);
		log.info(customer.getCode() + ": " + customer.getCreated().toString());
		
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		customer.setPhone("081567");
		customerDao.save(customer);
//		Assertions.assertNotEquals(customer.getCreated(), customer.getModified());
		log.info(customer.getCode() + ",  created: " + customer.getCreated().toString());
		log.info(customer.getCode() + ", modified: " + customer.getModified().toString());
		log.error("sama, tapi yang tersimpan didatabase beda created dan modifiednya");
		
	}

}
