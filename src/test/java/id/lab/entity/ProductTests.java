package id.lab.entity;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import id.lab.dao.ProductDao;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootTest
public class ProductTests {
	
	@Autowired ProductDao productDao;
	
	@Test
	void updateProduct() {
		Product product = new Product();
		product.setCode("ANTASID");
		product.setName("Mylanta");
		product.setBarcode("8 992725 051219");
//		Assertions.assertNull(product.getCreated());
		productDao.save(product);
//		Assertions.assertNotNull(product.getCreated());
//		log.info(product.getCode() + ": " + product.getCreated().toString());
		product.setName("Mylanta Tablet");
		productDao.save(product);
		
	}

}
