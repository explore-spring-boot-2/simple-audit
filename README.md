# Simple Audit

Tanpa UI, hanya test

Untuk mejalankan test: 
linux: `./mvnw test`
windows: `mvnw.cmd test`

Belajar audit menggunakan Spring Boot seperti, date created, created by, date modified, modified by

* Entitiy Customer contoh pakai `@PreUpdate`

* Entitiy Supplier contoh pakai JPA Auditing (ref: [Spring Data Jpa Audit Example](https://rashidi.github.io/spring-boot-data-audit/))
    - `AuditConfig`
    - `AuditorAwareImpl`
    
* Entity Product contoh pakai hibernate envers, menggunakan annotations `@Audited` setiap insert, update, delete akan tercatat pada table `product_aud`

```
<dependency>
    <groupId>org.hibernate</groupId>
    <artifactId>hibernate-envers</artifactId>
    <version>${hibernate.version}</version>
</dependency>
```

* Entity FixedAsset contoh implementasi soft delete, menggunakan anotations `@SQLDelete` dan `@Where`
